# Maven Archivaインストール手順

## 1. Maven Archivaインストール

docker操作ユーザで以下コマンドを実行する。

```bash
[root@localhost docker-maven-archiva]# sh archiva-install.sh
b82b7e73fa4a: Loading layer [==================================================>]   79.8MB/79.8MB
4a405665d8e9: Loading layer [==================================================>]  188.5MB/188.5MB
6c888405084d: Loading layer [==================================================>]  78.85MB/78.85MB
933eae4bad07: Loading layer [==================================================>]  78.85MB/78.85MB
Loaded image: maven-archiva:2.2.10
[+] Running 2/2
 ✔ Network docker-maven-archiva_default  Created                                              0.0s
 ✔ Container maven-archiva               Started                                              0.0s
```

## 2. dockerイメージロード確認

```bash
docker images
REPOSITORY         TAG           IMAGE ID       CREATED         SIZE
maven-archiva      2.2.10        acf662952c40   5 minutes ago   421MB
```

## 3. アクセス

```bash
http://localhost:8082/archiva
```

### 4. Maven Archivaコンテナ停止

docker操作ユーザで以下コマンドを実行する。

```bash
docker compose down
```

## 補足

- dockerイメージビルド

```bash
docker build -t maven-archiva:2.2.10 .
```

- dockerイメージ保存

```bash
docker save maven-archiva:2.2.10 | gzip > maven-archiva-2.2.10.tar.gz
```

- コンテナ起動時ログ確認

```bash
docker logs -f maven-archiva
```
