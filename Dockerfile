FROM ubuntu:24.04

# Install OpenJdk Java 11 SDK
RUN apt-get update && apt-get -y install openjdk-8-jdk-headless && rm -rf /var/lib/apt

ADD asset/apache-archiva-2.2.10-bin.tar.gz /tmp/
RUN mv /tmp/apache-archiva-2.2.10 /archiva
EXPOSE 8082
CMD /archiva/bin/archiva console
VOLUME ["/archiva/conf", "/archiva/data", "/archiva/repositories", "/archiva/logs", "/archiva/contexts"]
