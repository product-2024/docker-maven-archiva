#!/bin/sh
SCRIPT_DIR=$(cd $(dirname -- $0) && pwd)
IMAGE_FILE=maven-archiva-2.2.10.tar.gz

mkdir /var/opt/maven-archiva

cd ${SCRIPT_DIR}
docker load -i ${IMAGE_FILE}
docker compose up -d
